package utility;

import components.DataEditorComponent;
import datasource.database.DatabaseTable;
import mediators.Mediator;
import model.DataObject;

public abstract class DataEditor implements DataEditorComponent {
    private DataEditorMediator dataEditorMediator;

    DatabaseTable getTable(Class<? extends DataObject> type) {
        return dataEditorMediator.getTable(type);
    }

    DataEditorMediator getDataEditorMediator() {
        return dataEditorMediator;
    }

    @Override
    public void setMediator(Mediator mediator) {
        this.dataEditorMediator = (DataEditorMediator) mediator;
    }
}
