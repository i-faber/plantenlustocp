package utility;

import components.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateFormatter implements Component {
	public final DateTimeFormatter YEAR_LAST_DATE_FORMAT = DateTimeFormatter.ofPattern("d-M-yyyy");
	public final DateTimeFormatter YEAR_FIRST_DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-M-d");

	public String formatDateYearLast(LocalDate date) {
		return String.format("%1$td-%1$tm-%1$tY", date);
	}

	public String formatDateYearFirst(LocalDate date) {
		return String.format("%1$tY-%1$tm-%1$td", date);
	}
}
