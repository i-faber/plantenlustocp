package utility;

import components.Component;

import java.util.Scanner;

public class InputReader implements Component {

    private static final Scanner SCANNER = new Scanner(System.in);

    public int readInt(String message) {
        int choice = 0;
        boolean correctInput = false;
        while (!correctInput) {
            System.out.print(message);
            String input = SCANNER.nextLine();

            try {
                String[] values = input.split(" ");
                choice = Integer.parseInt(values[0]);
                correctInput = true;
            } catch (NumberFormatException e) {
                if (input.equalsIgnoreCase("esc")) {
                    break;
                }
                System.out.println("\t" + "Invoer is geen heel getal.");
                SCANNER.reset();
            }
        }
        return choice;
    }

    public String readString(String message) {
        System.out.print(message);
        return SCANNER.nextLine();
    }
}