package utility;

import components.Component;
import components.TwoWayComponent;
import components.UIComponent;
import datasource.database.DatabaseTable;
import mediators.Mediator;
import model.DataObject;
import model.ModelData;
import userInterface.UIMediator;

import java.util.SortedMap;

public class DataEditorMediator implements UIComponent, Mediator {
	private UIMediator uiMediator;
	private OrderEditor orderEditor;
	private InputReader inputReader;
	private DateFormatter dateFormatter;

    public boolean editOrderStatus(SortedMap<String, ? extends ModelData> map, int orderNr) {
        return orderEditor.editOrderStatus(map, orderNr);
    }

    public boolean editDeliveryDate(SortedMap<String, ? extends ModelData> map, int orderNr) {
        return orderEditor.editDeliveryDate(map, orderNr);
    }

    String readString(String message) {
        return inputReader.readString(message);
    }

    @Override
    public void registerComponent(Component component) {
        if (component instanceof TwoWayComponent) {
            ((TwoWayComponent) component).setMediator(this);
        }

        if (component instanceof OrderEditor) {
            orderEditor = (OrderEditor) component;
        } else if (component instanceof InputReader) {
            inputReader = (InputReader) component;
        } else if (component instanceof DateFormatter) {
            dateFormatter = (DateFormatter) component;
        }
    }

    DatabaseTable getTable(Class<? extends DataObject> type) {
        return uiMediator.getTable(type);
    }

    DateFormatter getDateFormatter() {
        return dateFormatter;
    }

    @Override
	public void setMediator(Mediator mediator) {
		this.uiMediator = (UIMediator) mediator;
	}
}
