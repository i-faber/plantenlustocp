package utility;

import datasource.database.DatabaseTable;
import datasource.database.dbTables.OrderTable;
import model.ModelData;
import model.Order;
import reference.StatusCode;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.SortedMap;

public class OrderEditor extends DataEditor {
    private static final String INVALID_ORDER_NR_MESSAGE = "\t" + "Ongeldig ordernummer";
    private static final String ORDER_NR_NOT_FOUND_MESSAGE = "\t" + "Ordernummer komt niet voor in huidige lijst.";

    public boolean editOrderStatus(SortedMap<String, ? extends ModelData> map, int orderNr) {
        if (isAcceptedOrderNr(map, orderNr)) {
            String status = getDataEditorMediator().readString("\t" + "Voer nieuwe status in: ");

            if ((status.length() == 1) && (StatusCode.getOrderCodes().contains(status.toUpperCase()))) {
                DatabaseTable table = getTable(Order.class);
                if (table instanceof OrderTable) {
                    ((OrderTable) table).updateOrderStatus(status.toUpperCase(), orderNr);
                }
                return true;
            } else {
                System.out.println("\t" + "Invoer is geen geldige statuscode! \n" +
                        "\t" + "Geldige statuscodes: " + StatusCode.getOrderCodes());
            }
        }
        return false;
    }

    public boolean editDeliveryDate(SortedMap<String, ? extends ModelData> map, int orderNr) {
        if (isAcceptedOrderNr(map, orderNr)) {
            ModelData data = map.get(String.valueOf(orderNr));
            LocalDate orderDate;
            if (data instanceof Order) {
                orderDate = ((Order) data).getOrderDate();
            } else {
                System.out.println("\t" + "Probleem met model: geselecteerd object is geen order");
                return false;
            }

            String input = getDataEditorMediator().readString("\t" + "Voer de nieuwe leverdatum (d-m-jjjj) in: ");
            LocalDate deliveryDate = null;
            try {
                deliveryDate = LocalDate.parse(input, getDataEditorMediator().getDateFormatter().YEAR_LAST_DATE_FORMAT);
            } catch (DateTimeParseException e) {
                System.out.println("\t" + "Ongeldige datum.");
            }

            if (deliveryDate != null) {
                OrderTable table;
                try {
                    table = ((OrderTable) getTable(Order.class));
                } catch (ClassCastException e) {
                    System.out.println("\t" + "Probleem met ophalen van database tabel.");
                    return false;
                }

//                LocalDate orderDate = LocalDate.parse(Objects.requireNonNull(table.fetchOrderDateByOrderNr(orderNr)),
//                        getDataEditorMediator().getDateFormatter().YEAR_FIRST_DATE_FORMAT);

                if (deliveryDate.isAfter(orderDate) || deliveryDate.equals(orderDate)) {
                    table.updateDeliveryDate(getDataEditorMediator().getDateFormatter().formatDateYearFirst(deliveryDate), orderNr);
                    return true;
                } else {
                    System.out.println("\t" + "Leverdatum moet na besteldatum liggen.");
                }
            }
        }
        return false;
    }

    private boolean isAcceptedOrderNr(SortedMap<String, ? extends ModelData> map, int orderNr) {
        if (orderNr != 0) {
            if (map.containsKey(String.valueOf(orderNr))) {
                return true;
            } else {
                System.out.println(ORDER_NR_NOT_FOUND_MESSAGE);
            }
        } else {
            System.out.println(INVALID_ORDER_NR_MESSAGE);
        }
        return false;
    }
}
