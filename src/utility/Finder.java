package utility;

import components.UIComponent;
import datasource.database.dbTables.GoodsReceiptTable;
import datasource.database.dbTables.OrderLineTable;
import datasource.database.dbTables.OrderTable;
import datasource.database.dbTables.PlantTable;
import mediators.Mediator;
import model.*;
import userInterface.UIMediator;

import java.util.SortedMap;

public class Finder implements UIComponent {
    private UIMediator uiMediator;

    public SortedMap<String, ? extends ModelData> fetchFullTable(Class<? extends DataObject> type) {
        return uiMediator.getTable(type).fetchTable();
    }

    public SortedMap<String, ? extends ModelData> findMatchingResults(Class<? extends DataObject> type, int searchKey) {
        SortedMap<String, ? extends ModelData> resultMap = null;
        if (type == Order.class) {
            resultMap = ((OrderTable) uiMediator.getTable(type)).findOrdersBySupplier(searchKey);
        } else if (type == OrderLine.class) {
            resultMap = ((OrderLineTable) uiMediator.getTable(type)).findOrderLines(searchKey);
        } else if (type == Plant.class) {
            resultMap = ((PlantTable) uiMediator.getTable(type)).findPlantByItemCode(searchKey);
        }

        return resultMap;
    }


    public SortedMap<String, ? extends ModelData> findMatchingResults(Class<? extends DataObject> type, int searchKey, int orderNr) {
        SortedMap<String, ? extends ModelData> resultMap = null;
        if (type == GoodsReceipt.class) {
            resultMap = ((GoodsReceiptTable) uiMediator.getTable(type)).findReceiptsByPrimaryKey(orderNr, searchKey);
        }
        return resultMap;
    }

    @Override
    public void setMediator(Mediator mediator) {
        this.uiMediator = (UIMediator) mediator;
    }
}
