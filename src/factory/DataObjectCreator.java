package factory;

import com.sun.istack.internal.NotNull;
import components.DatabaseComponent;
import datasource.database.DatabaseMediator;
import datasource.database.dbTables.PlantTable;
import mediators.Mediator;
import model.*;

public class DataObjectCreator implements DatabaseComponent {
    private DatabaseMediator databaseMediator;

    public Supplier create(int supplierCode, @NotNull String name, @NotNull String address, @NotNull String location) {
        return new Supplier(supplierCode, name, address, location);
    }

    public Order create(int orderNr, int supplierCode, @NotNull String orderDate, @NotNull String deliveryDate, double amount, @NotNull String status) {
        return new Order(orderNr, supplierCode, orderDate, deliveryDate, amount, status);
    }

    public OrderLine create(int orderNr, int itemCode, int quantity, double orderPrice) {
        String plantName = getPlantName(itemCode);
        return new OrderLine(orderNr, itemCode, quantity, orderPrice, plantName);
    }

    public Plant create(int itemCode, @NotNull String name, @NotNull String species, @NotNull String colour, int height,
                        int floweringStart, int floweringEnd, double price, int stockQuantity, int stockMin) {
        return new Plant(itemCode, name, species, colour, height, floweringStart, floweringEnd, price, stockQuantity, stockMin);
    }

    public GoodsReceipt create(int orderNr, int itemCode, @NotNull String dateOfReceipt, int receivedQuantity, @NotNull String status,
                               int entryCode, int serialNumber) {
        String plantName = getPlantName(itemCode);
        return new GoodsReceipt(orderNr, itemCode, dateOfReceipt, receivedQuantity, status, entryCode, serialNumber, plantName);
    }

    private String getPlantName(int itemCode) {
        try {
            return ((PlantTable) databaseMediator.getTable(Plant.class)).findPlantNameByItemCode(itemCode);
        } catch (Exception e) {
            System.out.println("Error retrieving plant name: " + e.getMessage());
            return "";
        }
    }

    @Override
    public void setMediator(Mediator mediator) {
        this.databaseMediator = (DatabaseMediator) mediator;
    }
}
