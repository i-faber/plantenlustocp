package factory;

import datasource.database.DatabaseMediator;
import datasource.database.SQLDatabaseMediator;
import datasource.database.dbTables.*;

public class DatabaseMediatorCreator {

    public static DatabaseMediator createForSQL(String dbLocation, String dbName) {
        DatabaseMediator databaseMediator = new SQLDatabaseMediator(dbLocation, dbName);
        databaseMediator.registerComponent(new DataObjectCreator());

        databaseMediator.registerTable(new SupplierTable());
        databaseMediator.registerTable(new OrderTable());
        databaseMediator.registerTable(new OrderLineTable());
        databaseMediator.registerTable(new PlantTable());
        databaseMediator.registerTable(new GoodsReceiptTable());

        return databaseMediator;
    }
}