package factory;

import userInterface.menu.*;
import utility.InputReader;

public class MenuMediatorCreator {

    public static MenuMediator createForModelData() {
        MenuMediator menuMediator = new ModelMenuMediator();
        menuMediator.registerComponent(new InputReader());

        menuMediator.registerComponent(new MainMenu());
        menuMediator.registerComponent(new SupplierMenu());
        menuMediator.registerComponent(new OrderMenu());
        menuMediator.registerComponent(new OrderLineMenu());
        menuMediator.registerComponent(new PlantMenu());
        menuMediator.registerComponent(new GoodsReceiptMenu());
        return menuMediator;
    }
}
