package factory;

import utility.DataEditorMediator;
import utility.DateFormatter;
import utility.InputReader;
import utility.OrderEditor;

public class DataEditorCreator {
    public static DataEditorMediator create() {
        DataEditorMediator dataEditorMediator = new DataEditorMediator();
        dataEditorMediator.registerComponent(new OrderEditor());
        dataEditorMediator.registerComponent(new InputReader());
        dataEditorMediator.registerComponent(new DateFormatter());
        return dataEditorMediator;
    }
}
