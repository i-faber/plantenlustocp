package view;

public class ModelStyle {
    private static final String[] HEADERS_TO_BE_RIGHT_ALIGNED = {"CODE", "NR", "BOEKSTUK", "DATUM", "BEDRAG", "PRIJS", "TOTAAL", "AANTAL", "HOOGTE", "VOORRAAD", "STATUS"};
    private static final int NUM_CELL_PADDING = 4;

    public int getNumCellPadding() { return NUM_CELL_PADDING; }

    public boolean isRightAligned(String headerOfColumn) {
        String moddedHeaderOfColumn = headerOfColumn.toUpperCase();

        for (String header : HEADERS_TO_BE_RIGHT_ALIGNED) {
            if (moddedHeaderOfColumn.contains(header)) {
                return true;
            }
        }
        return false;
    }
}