package view;

import model.ModelData;

import java.util.SortedMap;

public class ModelViewer extends Viewer{
    private final ModelStyle modelStyle;

    private String title;
    private String[] header;
    private int[] maxColumnWidths;

    private static final String NO_DATA_MESSAGE = "Geen gegevens om weer te geven";

    public ModelViewer(ModelStyle modelStyle) {
        this.modelStyle = modelStyle;
    }

    @Override
    public boolean viewModel(SortedMap<String,? extends ModelData> map) {
        if ((map != null) && (map.size() > 0)) {
            extractMetaData(map.get(map.firstKey()));
            generateTitle();
            generateHeader();

            int k = 0;
            for (SortedMap.Entry<String, ? extends ModelData> entry : map.entrySet()) {
                generateRowNumber(k);
                generateRow(entry.getValue().getData());
                k++;
            }
            return true;

        } else {
            System.out.println(NO_DATA_MESSAGE);
        }
        return false;
    }

    private void extractMetaData(ModelData sampleElement) {
        title = sampleElement.getTitle();
        header = sampleElement.getHeader();
        maxColumnWidths = sampleElement.getMaxColumnWidths();
    }

    private void generateTitle() {
        System.out.println("\n" + title + "\n");
    }

    private void generateHeader() {
        int totalHeaderLength = 0;
        int currentHeaderLength = 0;

        String columnZero = " || ";

        totalHeaderLength += (modelStyle.getNumCellPadding() + columnZero.length());

        for (int i = modelStyle.getNumCellPadding() + 1; i > 0; i--) {
            System.out.print(" ");
        }
        System.out.print(columnZero);

        for (int currentColIndex = 0; currentColIndex < header.length; currentColIndex++) {
            int headerLength = (header[currentColIndex]).length();

            StringBuilder emptySpaces = new StringBuilder();
            for (int i = maxColumnWidths[currentColIndex] - headerLength; i > 0; i--) {
                emptySpaces.append(" ");
            }

            String columnHeader = header[currentColIndex] + emptySpaces;

            currentHeaderLength += columnHeader.length();

            System.out.print(columnHeader);

            maxColumnWidths[currentColIndex] = currentHeaderLength;

            totalHeaderLength += currentHeaderLength;

            currentHeaderLength = 0;

            if (currentColIndex < header.length - 1) {
                String divider = " | ";
                totalHeaderLength += divider.length();
                System.out.print(divider);
            }
        }
        generateLine(totalHeaderLength);
    }

    private void generateLine(int totalHeaderLength) {
        System.out.println();
        for (int i = totalHeaderLength + 3; i > 0; i--) {
            System.out.print("-");
        }
        System.out.println();
    }

    private void generatePadding(int neededPadding) {
        for (int i = neededPadding; i > 0; i-- ) {
            System.out.print(" ");
        }
    }

    private void generateRowNumber(int currentElement) {
        int rowNumber = currentElement + 1;
        int neededPadding = modelStyle.getNumCellPadding();

        if (rowNumber >= 10 && rowNumber < 100) { neededPadding -= 1; }
        else if (rowNumber >= 100 && rowNumber < 1000) { neededPadding -= 2; }
        else if (rowNumber >= 1000 && rowNumber < 10000) { neededPadding -= 3; }
        else if (rowNumber >= 10000) { neededPadding -= 4; }

        generatePadding(neededPadding);
        System.out.print(rowNumber + " || ");
    }

    private void generateRow(String[] data) {
        for (int currentColIndex = 0; currentColIndex < data.length; currentColIndex++) {
            int neededPadding = maxColumnWidths[currentColIndex] - data[currentColIndex].length();

            if (modelStyle.isRightAligned(header[currentColIndex])) {
                generatePadding(neededPadding);
                System.out.print(data[currentColIndex]);
            } else {
                System.out.print(data[currentColIndex]);
                generatePadding(neededPadding);
            }

            if (currentColIndex < data.length - 1) {
                System.out.print(" | ");
            }
        }
        System.out.println();
    }
}