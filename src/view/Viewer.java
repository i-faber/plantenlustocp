package view;

import components.Component;
import model.ModelData;

import java.util.SortedMap;

public abstract class Viewer implements Component {

    public abstract boolean viewModel(SortedMap<String,? extends ModelData> map);

}
