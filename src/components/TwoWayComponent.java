package components;

import mediators.Mediator;

public interface TwoWayComponent extends Component {
    void setMediator(Mediator mediator);
}
