package model;

import com.sun.istack.internal.NotNull;

import java.time.LocalDate;

public class GoodsReceipt extends ModelData {
    private static int[] maxColumnWidths;

    private final int orderNr; // KEY
    private final int itemCode; // KEY
    private final LocalDate dateOfReceipt; // KEY
    private final int receivedQuantity;
    private final String status;
    private final int entryCode;
    private final int serialNumber;
    private final String plantName;

    public GoodsReceipt(int orderNr, int itemCode, @NotNull String dateOfReceipt, int receivedQuantity, @NotNull String status,
                        int entryCode, int serialNumber, String plantName) throws IllegalArgumentException {
        if (orderNr < 0 || itemCode < 0) {
            throw new IllegalArgumentException("Order number and Item code cannot be less than zero.");
        }

        this.orderNr = orderNr;
        this.itemCode = itemCode;
        this.dateOfReceipt = getModelManager().convertToLocalDate(dateOfReceipt);
        this.receivedQuantity = receivedQuantity;
        this.status = status;
        this.entryCode = entryCode;
        this.serialNumber = serialNumber;

        this.plantName = plantName;

        if (maxColumnWidths == null) {
            maxColumnWidths = new int[getModelManager().getHeader(this).length];
        }
        maxColumnWidths = logMaxColumnWidth(getData(), maxColumnWidths);
    }

    // Getters
    @Override
    public int getKey() {
        return serialNumber;
    }

    @Override
    public int[] getMaxColumnWidths() {
        return maxColumnWidths;
    }

    @Override
    public String[] getData() {
        return new String[]{
                Integer.toString(orderNr),
                Integer.toString(itemCode),
                plantName,
                getModelManager().formatDate(dateOfReceipt),
                Integer.toString(receivedQuantity),
                status,
                Integer.toString(entryCode),
                Integer.toString(serialNumber) };
    }

    @Override
    public String toString() {
        return "orderNr=" + orderNr +
                ", itemCode=" + itemCode +
                ", plantName='" + plantName + '\'' +
                ", dateOfReceipt=" + dateOfReceipt +
                ", receivedQuantity=" + receivedQuantity +
                ", status='" + status + '\'' +
                ", entryCode=" + entryCode +
                ", serialNumber=" + serialNumber;
    }
}