package model;

public class OrderLine extends ModelData {
	private static int[] maxColumnWidths;

	private final int orderNr; // KEY
	private final int itemCode; // KEY
	private final String plantName;
	private final int quantity;
	private final double orderPrice;
	private final double lineTotal;
	
	public OrderLine(int orderNr, int itemCode, int quantity, double orderPrice, String plantName) throws IllegalArgumentException {
		if (orderNr < 0 || itemCode < 0) {
			throw new IllegalArgumentException("Order Number and Item Code cannot be less than zero.");
		}

		this.orderNr = orderNr;
		this.itemCode = itemCode;
		this.quantity = quantity;
		this.orderPrice = orderPrice;
		this.lineTotal = orderPrice * quantity;

		this.plantName = plantName;

		if (maxColumnWidths == null) {
			maxColumnWidths = new int[getModelManager().getHeader(this).length];
		}
		maxColumnWidths = logMaxColumnWidth(getData(), maxColumnWidths);
	}

	// Getters
	@Override
	public int getKey() { return orderNr; }

	@Override
	public int[] getMaxColumnWidths() { return maxColumnWidths;	}

	@Override
	public String[] getData() {
		return new String[]{
				Integer.toString(orderNr),
				Integer.toString(itemCode),
				plantName,
				Integer.toString(quantity),
				getModelManager().formatMoney(orderPrice),
				getModelManager().formatMoney(lineTotal),
//				String.format(ModelStyle.CURRENCY_FORMAT, lineTotal)
				};
	}
}
