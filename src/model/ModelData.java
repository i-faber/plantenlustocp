package model;

public abstract class ModelData extends DataObject implements Comparable<ModelData>{
    private final ModelManager modelManager;

    // Getters
    public abstract int getKey();
    public abstract int[] getMaxColumnWidths();

    ModelData() {
        modelManager = ModelManager.getInstance();
    }

    ModelManager getModelManager() {
        return modelManager;
    }

    public String getTitle() {
        return modelManager.getTitle(this);
    }

    public String[] getHeader() {
        return modelManager.getHeader(this);
    }

    int[] logMaxColumnWidth(String[] data, int[] maxColumnWidths) {
        for(int i = 0; i < data.length; i++) {
            maxColumnWidths[i] = Integer.max(data[i].length(), maxColumnWidths[i]);
        }
        return maxColumnWidths;
    }

    @Override
    public int compareTo(ModelData other) {
        return Integer.compare(this.getKey(), other.getKey());
    }
}
