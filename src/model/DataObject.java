package model;

public abstract class DataObject {
    public abstract String[] getData();
}
