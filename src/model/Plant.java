package model;

import com.sun.istack.internal.NotNull;

public class Plant extends ModelData {
	private static int[] maxColumnWidths;

	private final int itemCode; // KEY
	private final String name;
	private final String species;
	private final String colour;
	private final int height;
	private final int floweringStart;
	private final int floweringEnd;
	private final double price;
	private final int stockQuantity;
	private final int stockMin;
	
	public Plant(int itemCode, @NotNull String name, @NotNull String species, @NotNull String colour, int height,
				 int floweringStart, int floweringEnd, double price, int stockQuantity, int stockMin) throws IllegalArgumentException {
		if (itemCode < 0) {
			throw new IllegalArgumentException("Item Code cannot be less than zero");
		}

		this.itemCode = itemCode;
		this.name = name;
		this.species = species;
		this.colour = colour;
		this.height = height;
		this.floweringStart = floweringStart;
		this.floweringEnd = floweringEnd;
		this.price = price;
		this.stockQuantity = stockQuantity;
		this.stockMin = stockMin;

		if (maxColumnWidths == null) {
			maxColumnWidths = new int[getModelManager().getHeader(this).length];
		}
		maxColumnWidths = logMaxColumnWidth(getData(), maxColumnWidths);
	}

	// Getters

	@Override
	public int getKey() { return itemCode; }

	@Override
	public int[] getMaxColumnWidths() { return maxColumnWidths; }
	
	public String[] getData() {
		return new String[]{
				Integer.toString(itemCode),
				name, species, colour,
				height != -1 ? Integer.toString(height) : "",
				floweringStart != -1 ? Integer.toString(floweringStart) : "",
				floweringEnd != -1 ? Integer.toString(floweringEnd) : "",
				price != -1 ? getModelManager().formatMoney(price) : "",
				stockQuantity != -1 ? Integer.toString(stockQuantity) : "",
				stockMin != -1 ? Integer.toString(stockMin) : ""};
	}

	@Override
	public String toString() {
		return  itemCode + " | " +
				name + " | " +
				species + " | " +
				colour + " | " +
				height + " | " +
				floweringStart + " | " +
				floweringEnd + " | " +
				getModelManager().formatMoney(price) + " | " +
//				ModelStyle.CURRENCY + " " + price + " | " +
				stockQuantity + " | " +
				stockMin;
	}
}