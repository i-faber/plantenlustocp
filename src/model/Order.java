package model;

import com.sun.istack.internal.NotNull;

import java.time.LocalDate;

public class Order extends ModelData {
	private static int[] maxColumnWidths;

	private final int orderNr; // KEY
	private final int supplierCode;
	private final LocalDate orderDate;
	private final LocalDate deliveryDate;
	private final double amount;
	private final String status;

	public Order(int orderNr, int supplierCode, @NotNull String orderDate, @NotNull String deliveryDate, double amount, @NotNull String status) throws IllegalArgumentException {
		if (orderNr < 0 || supplierCode < 0) {
			throw new IllegalArgumentException("Order number and supplier code cannot be less than zero.");
		}

		this.orderNr = orderNr;
		this.supplierCode = supplierCode;
		this.orderDate = getModelManager().convertToLocalDate(orderDate);
		this.deliveryDate = getModelManager().convertToLocalDate(deliveryDate);
		this.amount = amount;
		this.status = status;

		if (maxColumnWidths == null) {
			maxColumnWidths = new int[getModelManager().getHeader(this).length];
		}
		maxColumnWidths = logMaxColumnWidth(getData(), maxColumnWidths);
	}

	public LocalDate getOrderDate() {
		return orderDate;
	}

	// Getters
	@Override
	public int getKey() { return orderNr; }

	@Override
	public int[] getMaxColumnWidths() { return maxColumnWidths;	}

	@Override
	public String[] getData() {
		return new String[]{
			Integer.toString(orderNr),
			Integer.toString(supplierCode),
			getModelManager().formatDate(orderDate),
			getModelManager().formatDate(deliveryDate),
			getModelManager().formatMoney(amount),
			status
		};
	}

	@Override
	public String toString() {
		return 	orderNr + " | " +
				supplierCode + " | " +
				getModelManager().formatDate(orderDate) + " | " +
				getModelManager().formatDate(deliveryDate) + " | " +
				getModelManager().formatMoney(amount) + " | " +
				status;
	}

    public int getSupplierCode() {
		return supplierCode;
    }
}
