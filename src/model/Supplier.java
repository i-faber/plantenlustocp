package model;

import com.sun.istack.internal.NotNull;

public class Supplier extends ModelData {
	private static int[] maxColumnWidths;

	private final int supplierCode; // KEY
	private final String name;
	private final String address;
	private final String location;
		
	public Supplier(int supplierCode, @NotNull String name, @NotNull String address, @NotNull String location) throws IllegalArgumentException {
		if (supplierCode < 0) {
			throw new IllegalArgumentException("Supplier Code cannot be less than zero.");
		}

		this.supplierCode = supplierCode;
		this.name = name;
		this.address = address;
		this.location = location;

		if (maxColumnWidths == null) {
			maxColumnWidths = new int[getModelManager().getHeader(this).length];
		}
		maxColumnWidths = logMaxColumnWidth(getData(), maxColumnWidths);
	}

	// Getters
	@Override
	public int getKey() { return supplierCode; }

	@Override
	public int[] getMaxColumnWidths() { return maxColumnWidths; }

	@Override
	public String[] getData() {
		return new String[]{ Integer.toString(supplierCode), name, address, location};
	}

	@Override
	public String toString() {
		return "Supplier{" +
				"supplierCode=" + supplierCode +
				", name='" + name + '\'' +
				", address='" + address + '\'' +
				", location='" + location + '\'' +
				'}';
	}

	public int getSupplierCode() { return supplierCode; }
	public String getName() { return name; }
}