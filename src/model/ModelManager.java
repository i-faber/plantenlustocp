package model;

import utility.DateFormatter;

import java.time.LocalDate;

public class ModelManager {
    private static volatile ModelManager modelManager;
    private DateFormatter dateFormatter;

    private static final String CURRENCY = "€";
    private static final String CURRENCY_FORMAT = "%.2f";

    /** TITLES */
    private static final String SUPPLIER_TITLE = "LEVERANCIERS";
    private static final String ORDER_TITLE = "ORDERS";
    private static final String ORDER_LINE_TITLE = "ORDERREGELS";
    private static final String PLANT_TITLE = "PLANTEN";
    private static final String GOODS_RECEIPT_TITLE = "GOEDERENONTVANGST";

    /** HEADERS */
    private static final String[] SUPPLIERS_HEADER = {
            "Lev.code", "Leverancier", "Adres", "Plaats" };

    private static final String[] ORDERS_HEADER = {
            "Ordernr", "Lev.code", "Besteldatum", "Leverdatum", "Bedrag (" + CURRENCY + ")", "Status", };

    private static final String[] ORDER_LINES_HEADER = {
            "Ordernr", "Art.code", "Artikelnaam", "Aantal", "Bestelprijs (" + CURRENCY + ")", "Regeltotaal (" + CURRENCY + ")" };

    private static final String[] PLANT_HEADER = {
            "Art.code", "Artikelnaam", "Soort", "Kleur", "Hoogte (cm)", "Bloei begin", "Bloei eind",
            "Prijs (" + CURRENCY + ")", "Voorraad aantal", "Voorraad min."};

    private static final String[] GOODS_RECEIPT_HEADER = {
            "Ordernr", "Art.code", "Artikelnaam", "Ontvangstdatum", "Ontv. aantal", "Status",
            "Boekstuk", "Volgnr" };


    private ModelManager() {}

    public static ModelManager getInstance() {
        if (modelManager == null) {
            synchronized (ModelManager.class) {
                if (modelManager == null) {
                    modelManager = new ModelManager();
                    modelManager.dateFormatter = new DateFormatter();
                }
            }
        }
        return modelManager;
    }

    LocalDate convertToLocalDate(String date) {
        return LocalDate.parse(date, dateFormatter.YEAR_FIRST_DATE_FORMAT);
    }

    String formatDate(LocalDate date) {
        return dateFormatter.formatDateYearLast(date);
    }

    String formatMoney(double amount) {
        return String.format(CURRENCY_FORMAT, amount);
    }

    String getTitle(ModelData object) {

        if (object instanceof Supplier) {
            return SUPPLIER_TITLE;
        } else if (object instanceof Order) {
            return ORDER_TITLE;
        } else if (object instanceof OrderLine) {
            return ORDER_LINE_TITLE;
        } else if (object instanceof Plant) {
            return PLANT_TITLE;
        } else if (object instanceof GoodsReceipt) {
            return GOODS_RECEIPT_TITLE;
        }
        return "";
    }

    String[] getHeader(ModelData object) {
        if (object instanceof Supplier) {
            return SUPPLIERS_HEADER;
        } else if (object instanceof Order) {
            return ORDERS_HEADER;
        } else if (object instanceof OrderLine) {
            return ORDER_LINES_HEADER;
        } else if (object instanceof Plant) {
            return PLANT_HEADER;
        } else if (object instanceof GoodsReceipt) {
            return GOODS_RECEIPT_HEADER;
        }
        return new String[] {""};
    }
}
