package userInterface.menu;

import model.DataObject;
import model.GoodsReceipt;
import model.ModelData;
import model.Plant;
import reference.GlobalSymbols;

import java.util.Map;
import java.util.SortedMap;

abstract class ModelMenu extends Menu {

    void loadRequestedData(Class<? extends DataObject> dataType, Map<MessageTypes, String> messages){
        int searchKey = getMenuMediator().readInt(messages.get(MessageTypes.INPUT));

        if (searchKey != 0) {
            if (hasMatch(dataType, searchKey)) {
                SortedMap<String, ? extends ModelData> results = getResults(dataType, searchKey);

                if (results != null && results.size() > 0) {
                    getMenuMediator().getUiMediator().goToModel(results, String.valueOf(searchKey));
                } else {
                    System.out.println(messages.get(MessageTypes.NO_DATA));
                    this.loadMenu();
                }
            } else {
                System.out.println(messages.get(MessageTypes.NO_MATCH));
                this.loadMenu();
            }
        } else {
            this.loadMenu();
        }
    }


    private boolean hasMatch(Class<? extends DataObject> dataType, int searchKey) {
        if (dataType.equals(Plant.class) || dataType.equals(GoodsReceipt.class)) {
            return getMenuMediator().getUiMediator().getCurrentModel().containsKey(
                    getMenuMediator().getUiMediator().getLastUsedKey() + GlobalSymbols.DELIMITER + searchKey);
        } else {
            return getMenuMediator().getUiMediator().getCurrentModel().containsKey(String.valueOf(searchKey));
        }
    }

    private SortedMap<String, ? extends ModelData> getResults(Class<? extends DataObject> dataType, int searchKey) {
        if (dataType.equals(GoodsReceipt.class)) {
            int orderNr = getMenuMediator().getUiMediator().getFirstKeyOfCurrentModel();
            return getMenuMediator().getUiMediator().findMatchingResults(dataType, searchKey, orderNr);
        } else {
            return getMenuMediator().getUiMediator().findMatchingResults(dataType, searchKey);
        }
    }

    void loadMainMenu() {
        getMenuMediator().getUiMediator().goToMainMenu();
    }

    void goBack() {
        getMenuMediator().getUiMediator().navigateBack();
    }
}
