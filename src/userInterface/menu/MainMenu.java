package userInterface.menu;

import model.Order;
import model.Supplier;

public class MainMenu extends Menu {
	private static final Class ASSOCIATED_TYPE = MainMenu.class;
	private static final String[] OPTIONS = {
			" 1. Overzicht leveranciers",
			" 2. Overzicht orders",
			"10. Beëindig programma" };

	@Override
	public void loadMenu() {
		showOptions(OPTIONS);
		
		boolean validChoice = false;	
		while (!validChoice) {
			int choice = getMenuMediator().readInt(MAKE_CHOICE_MESSAGE);
			
			switch (choice) {
			case 1: 
				viewSuppliers();
				validChoice = true; 
				break;
			case 2: 
				viewOrders();
				validChoice = true;
				break;
			case 10:
				endProgram();
				validChoice = true;
				break;
			default: 
				System.out.println(INVALID_CHOICE_MESSAGE);
				break;
			}	
		}
	}

	@Override
	public Class getAssociatedType() {
		return ASSOCIATED_TYPE;
	}

	private void viewSuppliers() {
		getMenuMediator().getUiMediator().goToModel(Supplier.class);
	}

	private void viewOrders() {
		getMenuMediator().getUiMediator().goToModel(Order.class);
	}

	private void endProgram() {
		getMenuMediator().endProgram();
	}
}
