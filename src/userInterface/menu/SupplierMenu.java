package userInterface.menu;

import model.Order;
import model.Supplier;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class SupplierMenu extends ModelMenu {
	private static final Class ASSOCIATED_TYPE = Supplier.class;
	private static final String[] OPTIONS = {
		" 1. Bekijk orders van leverancier",
		"10. Naar hoofdmenu" };

		private static final Map<MessageTypes, String> MESSAGES;

	static {
		Map<MessageTypes, String> tempMap = new HashMap<>();
		tempMap.put(MessageTypes.INPUT, "\t" + "Voer leveranciercode in: ");
		tempMap.put(MessageTypes.NO_DATA, "\t" + "Geen orders gevonden.");
		tempMap.put(MessageTypes.NO_MATCH, "\t" + "Leveranciercode komt niet voor in huidige lijst.");
		MESSAGES = Collections.unmodifiableMap(tempMap);
	}

	@Override
	public void loadMenu() {
		showOptions(OPTIONS);
		
		boolean validChoice = false;	
		while (!validChoice) {
			int choice = getMenuMediator().readInt(MAKE_CHOICE_MESSAGE);

			switch (choice) {
				case 1: 
					viewOrderList(); 
					validChoice = true;
					break;
				case 10:
					loadMainMenu();
					validChoice = true;
					break;
				default: 
					System.out.println(INVALID_CHOICE_MESSAGE);
					break;
			}
		}
	}

	private void viewOrderList() {
		loadRequestedData(Order.class, MESSAGES);
	}

	@Override
	public Class getAssociatedType() {
		return ASSOCIATED_TYPE;
	}
}
