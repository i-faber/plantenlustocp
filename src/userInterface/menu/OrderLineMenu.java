package userInterface.menu;

import model.GoodsReceipt;
import model.OrderLine;
import model.Plant;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class OrderLineMenu extends ModelMenu {
	private static final Class ASSOCIATED_TYPE = OrderLine.class;

	private static final String[] OPTIONS = {
			" 1. Bekijk artikel",
			" 2. Bekijk goederontvangst",
			" 9. Terug",
			"10. Naar hoofdmenu" };

	private static final String INSERT_ITEM_CODE_MESSAGE = "\t" + "Voer artikelcode in: ";
	private static final String ITEM_CODE_NOT_IN_LIST_MESSAGE = "\t" + "Artikelcode komt niet voor in huidige lijst";

	private static final Map<MessageTypes, String> PLANT_MESSAGES;

	static {
		Map<MessageTypes, String> tempMap = new HashMap<>();
		tempMap.put(MessageTypes.INPUT, INSERT_ITEM_CODE_MESSAGE);
		tempMap.put(MessageTypes.NO_DATA, "\t" + "Artikel niet gevonden.");
		tempMap.put(MessageTypes.NO_MATCH, ITEM_CODE_NOT_IN_LIST_MESSAGE);
		PLANT_MESSAGES = Collections.unmodifiableMap(tempMap);
	}

	private static final Map<MessageTypes, String> GOODS_MESSAGES;
	static {
		Map<MessageTypes, String> tempMap = new HashMap<>();
		tempMap.put(MessageTypes.INPUT, INSERT_ITEM_CODE_MESSAGE);
		tempMap.put(MessageTypes.NO_DATA, "\t" + "Geen goederenontvangst gevonden.");
		tempMap.put(MessageTypes.NO_MATCH, ITEM_CODE_NOT_IN_LIST_MESSAGE);
		GOODS_MESSAGES = Collections.unmodifiableMap(tempMap);
	}

	public void loadMenu() {
		showOptions(OPTIONS);
		
		boolean validChoice = false;	
		while (!validChoice) {
			int choice = getMenuMediator().readInt(MAKE_CHOICE_MESSAGE);
		
			switch (choice) {
				case 1:
					viewPlants();
					validChoice = true;
					break;
				case 2:
					viewGoodsReceiptList();
					validChoice = true;
					break;
				case 9:
					goBack();
					validChoice = true;
					break;
				case 10:
					loadMainMenu();
					validChoice = true;
					break;
				default:
					System.out.println(INVALID_CHOICE_MESSAGE);
					break;
			}
		}
	}

	@Override
	public Class getAssociatedType() {
		return ASSOCIATED_TYPE;
	}

	private void viewPlants() {
		loadRequestedData(Plant.class, PLANT_MESSAGES);
	}
	
	private void viewGoodsReceiptList() {
		loadRequestedData(GoodsReceipt.class, GOODS_MESSAGES);
	}
}
