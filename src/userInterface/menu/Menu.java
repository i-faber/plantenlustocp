package userInterface.menu;

import components.MenuComponent;
import mediators.Mediator;

public abstract class Menu implements MenuComponent {
    static final String MAKE_CHOICE_MESSAGE = "Maak een keus: ";
    static final String INVALID_CHOICE_MESSAGE = "\t" + "Ongeldige keus";

    enum MessageTypes {
        INPUT, NO_DATA, NO_MATCH
    }

    private MenuMediator menuMediator;

    public abstract void loadMenu();

    @Override
    public void setMediator(Mediator mediator) {
        this.menuMediator = (MenuMediator) mediator;
    }

    MenuMediator getMenuMediator() {
        return menuMediator;
    }

    abstract public Class getAssociatedType();

    void showOptions(String[] options) {
        System.out.println();

        for (String option : options) {
            System.out.println(option);
        }
    }
}
