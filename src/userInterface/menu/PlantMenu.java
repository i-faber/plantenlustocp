package userInterface.menu;

import model.Plant;

public class PlantMenu extends ModelMenu {
	private static final Class ASSOCIATED_TYPE = Plant.class;
	private static final String[] OPTIONS = {
		" 1. Terug",
		"10. Naar hoofdmenu" };

	public void loadMenu() {
		showOptions(OPTIONS);
		
		boolean validChoice = false;	
		while (!validChoice) {
			int choice = getMenuMediator().readInt(MAKE_CHOICE_MESSAGE);
			
			switch (choice) {
				case 1: 
					goBack();
					validChoice = true;
					break;
				case 10:
					loadMainMenu();
					validChoice = true; 
					break;
				default: 
					System.out.println(INVALID_CHOICE_MESSAGE);
					break;
			}
		}
	}

	@Override
	public Class getAssociatedType() {
		return ASSOCIATED_TYPE;
	}

}
