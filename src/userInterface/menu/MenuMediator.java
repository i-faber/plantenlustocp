package userInterface.menu;

import com.sun.istack.internal.NotNull;
import components.Component;
import components.MenuComponent;
import components.TwoWayComponent;
import components.UIComponent;
import mediators.Mediator;
import model.ModelData;
import userInterface.UIMediator;
import utility.InputReader;

import java.util.HashMap;
import java.util.Map;

public abstract class MenuMediator implements UIComponent, Mediator {
    private UIMediator uiMediator;
    private InputReader inputReader;

    final Map<Class, Menu> menus = new HashMap<>();

    @Override
    public void registerComponent(Component component) {
        if (component instanceof TwoWayComponent) {
            ((TwoWayComponent) component).setMediator(this);
        }

        if (component instanceof MenuComponent) {
            Menu menu = (Menu) component;
            menus.put(menu.getAssociatedType(), menu);
            menu.setMediator(this);
        } else if (component instanceof InputReader) {
            inputReader = (InputReader) component;
        }
    }

    public abstract void loadMenu(@NotNull Class associatedType);

    public abstract void loadMenuOfCurrent();

    boolean editOrderStatus(int orderNr) {
        return getUiMediator().getDataEditorMediator().editOrderStatus(uiMediator.getCurrentModel(), orderNr);
    }

    boolean editDeliveryDate(int orderNr) {
        return getUiMediator().getDataEditorMediator().editDeliveryDate(uiMediator.getCurrentModel(), orderNr);
    }

    void updateCurrentModel(Class<? extends ModelData> ASSOCIATED_TYPE, int key) {
        getUiMediator().updateCurrentModel(ASSOCIATED_TYPE, key);
    }

    int readInt(String message) {
        return inputReader.readInt(message);
    }

    UIMediator getUiMediator() {
        return uiMediator;
    }

    @Override
    public void setMediator(Mediator mediator) {
        this.uiMediator = (UIMediator) mediator;
    }

    public void endProgram() {
        uiMediator.closeDbConnection();
        System.out.println("Programma beëindigd.");
        System.exit(0);
    }
}
