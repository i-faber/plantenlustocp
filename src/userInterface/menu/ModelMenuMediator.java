package userInterface.menu;

import com.sun.istack.internal.NotNull;
import model.ModelData;

import java.util.SortedMap;

public class ModelMenuMediator extends MenuMediator {

    public void loadMenuOfCurrent() {
        SortedMap<String, ? extends ModelData> currentMap = getUiMediator().getCurrentModel();
        Class type = currentMap
                .get(currentMap.firstKey())
                .getClass();
        loadMenu(type);
    }

    @Override
    public void loadMenu(@NotNull Class associatedType) {
        if (associatedType != null) {
            menus.get(associatedType).loadMenu();
        }
    }
}
