package userInterface.menu;

import model.Order;
import model.OrderLine;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class OrderMenu extends ModelMenu {
	private static final Class ASSOCIATED_TYPE = Order.class;
	private static final String[] OPTIONS = {
			" 1. Bekijk orderregels van order",
			" 2. Wijzig status van order",
			" 3. Wijzig leverdatum van order",
			" 9. Terug",
			"10. Naar hoofdmenu" };

	private static final String INSERT_ORDER_NR_MESSAGE = "\t" + "Voer het ordernummer van de te wijzigen order in: ";
	private static final String ERROR_EDIT_ORDER_MESSAGE = "\t" + "Order kon niet gewijzigd worden.";

	private static final Map<MessageTypes, String> MESSAGES;

	static {
			Map<MessageTypes, String> tempMap = new HashMap<>();
			tempMap.put(MessageTypes.INPUT, "\t" + "Voer ordernummer in: ");
			tempMap.put(MessageTypes.NO_DATA, "\t" + "Geen orderregels gevonden.");
			tempMap.put(MessageTypes.NO_MATCH, "\t" + "Ordernummer komt niet voor in huidige lijst.");
			MESSAGES = Collections.unmodifiableMap(tempMap);
		}

	public void loadMenu() {
		showOptions(OPTIONS);
		
		boolean validChoice = false;	
		while (!validChoice) {
			int choice = getMenuMediator().readInt(MAKE_CHOICE_MESSAGE);
		
			switch (choice) {
				case 1:
					viewOrderLineList();
					validChoice = true;
					break;
				case 2:
					editOrderStatus();
					validChoice = true;
					break;
				case 3:
					editDeliveryDate();
					validChoice = true;
					break;
				case 9:
					goBack();
					validChoice = true;
					break;
				case 10:
					loadMainMenu();
					validChoice = true;
					break;
				default: 
					System.out.println(INVALID_CHOICE_MESSAGE);
					break;
			}
		}
	}

	@Override
	public Class getAssociatedType() {
		return ASSOCIATED_TYPE;
	}

	private void viewOrderLineList() {
		loadRequestedData(OrderLine.class, MESSAGES);
	}

	private void editOrderStatus() {
		int orderNr = getMenuMediator().readInt(INSERT_ORDER_NR_MESSAGE);
		if (orderNr != 0) {
			boolean successful = getMenuMediator().editOrderStatus(orderNr);
			if (successful) {
				updateCurrentModel(orderNr);
			} else {
				System.out.println(ERROR_EDIT_ORDER_MESSAGE);
				this.loadMenu();
			}
		} else {
			this.loadMenu();
		}
	}

	private void editDeliveryDate() {
		int orderNr = getMenuMediator().readInt(INSERT_ORDER_NR_MESSAGE);
		if (orderNr != 0) {
			boolean successful = getMenuMediator().editDeliveryDate(orderNr);
			if (successful) {
				updateCurrentModel(orderNr);
			} else {
				System.out.println(ERROR_EDIT_ORDER_MESSAGE);
				this.loadMenu();
			}
		} else {
			this.loadMenu();
		}
	}

	private void updateCurrentModel(int key) {
		getMenuMediator().updateCurrentModel(ASSOCIATED_TYPE, key);
	}
}