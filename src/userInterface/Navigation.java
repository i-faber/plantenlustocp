package userInterface;

import com.sun.istack.internal.NotNull;
import components.UIComponent;
import mediators.Mediator;
import model.ModelData;
import model.Order;
import userInterface.menu.MainMenu;

import java.util.ArrayDeque;
import java.util.EmptyStackException;
import java.util.SortedMap;

public class Navigation implements UIComponent {

    private final ArrayDeque<SortedMap<String, ? extends ModelData>> renderedMapStack = new ArrayDeque<>();
    private final ArrayDeque<String> keyStack = new ArrayDeque<>();

    private UIMediator uiMediator;

    // Getters
    public int getFirstKeyOfCurrentModel() {
        return getCurrentModel().get(getCurrentModel().firstKey()).getKey();
    }

    public SortedMap<String, ? extends ModelData> getCurrentModel() {
        if (renderedMapStack.size() > 0) {
            return renderedMapStack.peek();
        }
        return null;
    }

    public String getLastUsedKey() {
        if (keyStack.size() > 0) {
            return keyStack.peek();
        } else {
            return null;
        }
    }

    // Methods
    private void goToModel(@NotNull SortedMap<String, ? extends ModelData> map) {
        boolean hasRendered = uiMediator.viewModel(map);
        if (hasRendered) {
            renderedMapStack.push(map);
            uiMediator.loadMenu(map.get(map.firstKey()).getClass());
        } else {
            uiMediator.loadMenuOfCurrent();
        }
    }

    public void goToModel(@NotNull SortedMap<String, ? extends ModelData> map, String key) {
        addToKeyStack(key);
        goToModel(map);
    }

    public void updateCurrentModel(Class<? extends ModelData> type, int key) {
        SortedMap<String, ? extends ModelData> map = null;
        String lastUsedKey = null;
        if (renderedMapStack.size() > 0) {
            map = renderedMapStack.poll();

            if (keyStack.size() > 0) {
                lastUsedKey = keyStack.peek();
            }

            if (type.equals(Order.class)) {
                if (map.get(map.firstKey()) instanceof Order) {
                    if (lastUsedKey != null) {
                        map = replaceOrder(map, Integer.parseInt(lastUsedKey), key);
                    } else {
                        Order order = (Order) map.get(String.valueOf(key));
                        int foreignKey = order.getSupplierCode();
                        map = replaceOrder(map, foreignKey, key);
                    }
                }
            }
        }

        if (map != null) {
            goToModel(map);
        } else {
            goToModel(uiMediator.fetchFullTable(type));
        }
    }

    private SortedMap<String, Order> replaceOrder(SortedMap<String, ? extends ModelData> map, int foreignKey, int key) {
            SortedMap<String, Order> tempMap = (SortedMap<String, Order>) map;
            Order order = (Order) uiMediator.fetchRow(Order.class, foreignKey, key);
            tempMap.put(String.valueOf(key), order);

            return tempMap;
    }

    public void navigateBack() {
        try {
            renderedMapStack.poll();
            removeLastUsedKey();
            goToModel(renderedMapStack.poll());
        } catch (EmptyStackException e) {
            goToMainMenu();
        }
    }

    public void goToMainMenu() {
        renderedMapStack.clear();
        clearKeyStack();
        uiMediator.loadMenu(MainMenu.class);
    }

    private void clearKeyStack() {
        keyStack.clear();
    }

    private void removeLastUsedKey() {
        if (!keyStack.isEmpty()) {
            keyStack.poll();
        }
    }

    private void addToKeyStack(String key) {
        if (key != null) {
            keyStack.push(key);
        }
    }


    @Override
    public void setMediator(Mediator mediator) {
        this.uiMediator = (UIMediator) mediator;
    }
}