package userInterface;

import com.sun.istack.internal.NotNull;
import components.Component;
import components.TwoWayComponent;
import datasource.database.DatabaseMediator;
import datasource.database.DatabaseTable;
import datasource.database.dbTables.OrderTable;
import mediators.Mediator;
import model.DataObject;
import model.ModelData;
import model.Order;
import userInterface.menu.MenuMediator;
import utility.DataEditorMediator;
import utility.Finder;
import view.Viewer;

import java.util.SortedMap;

public class UIMediator implements Mediator {
    private Navigation navigation;
    private DatabaseMediator databaseMediator;
    private Finder finder;
    private DataEditorMediator dataEditorMediator;
    private MenuMediator menuMediator;
    private Viewer viewer;

    public DataEditorMediator getDataEditorMediator() {
        return dataEditorMediator;
    }

    @Override
    public void registerComponent(Component component) {
        if (component instanceof TwoWayComponent) {
            ((TwoWayComponent) component).setMediator(this);
        }

        if (component instanceof DatabaseMediator) {
            databaseMediator = (DatabaseMediator) component;
        } else if (component instanceof Navigation) {
            navigation = (Navigation) component;
        } else if (component instanceof Finder) {
            finder = (Finder) component;
        } else if (component instanceof Viewer) {
            viewer = (Viewer) component;
        } else if (component instanceof DataEditorMediator) {
            dataEditorMediator = (DataEditorMediator) component;
        } else if (component instanceof MenuMediator) {
            menuMediator = (MenuMediator) component;
        }
    }

    // Navigation
    public void goToModel(Class<? extends DataObject> type) {
        navigation.goToModel(finder.fetchFullTable(type), null);
    }
    public void goToModel(SortedMap<String, ? extends ModelData> map, String key) {
        navigation.goToModel(map, key);
    }
    public SortedMap<String, ? extends ModelData> getCurrentModel() {
        return navigation.getCurrentModel();
    }
    public String getLastUsedKey() {
        return navigation.getLastUsedKey();
    }

    public void navigateBack() {
        navigation.navigateBack();
    }
    public void updateCurrentModel(Class<? extends ModelData> type, int key) { navigation.updateCurrentModel(type, key); }

    public int getFirstKeyOfCurrentModel() {
        return navigation.getFirstKeyOfCurrentModel();
    }

    // Viewer
    public boolean viewModel(SortedMap<String,? extends ModelData> map) {
        return viewer.viewModel(map);
    }

    // Database
    public boolean closeDbConnection() { return databaseMediator.close(); }

    public DatabaseTable getTable(Class<? extends DataObject> type) {
        return databaseMediator.getTable(type);
    }

    public SortedMap<String, ? extends ModelData> fetchFullTable(Class<? extends DataObject> type) { return finder.fetchFullTable(type); }

    public SortedMap<String,? extends ModelData> findMatchingResults(Class<? extends DataObject> dataType, int searchKey) {
        return finder.findMatchingResults(dataType, searchKey);
    }

    public SortedMap<String,? extends ModelData> findMatchingResults(Class<? extends DataObject> dataType, int searchKey, int orderNr) {
        return finder.findMatchingResults(dataType, searchKey, orderNr);
    }

    public ModelData fetchRow(Class<? extends DataObject> type, int key1, int key2) {
        if (type == Order.class) {
            return ((OrderTable) databaseMediator.getTable(type)).findOrderBySupplierAndOrderNr(key1, key2);
        }
        return null;
    }

    // Menu
    public void loadMenu(@NotNull Class associatedType) {
        menuMediator.loadMenu(associatedType);
    }

    public void loadMenuOfCurrent() {
        menuMediator.loadMenuOfCurrent();
    }

    public void goToMainMenu() {
        navigation.goToMainMenu();
    }
}
