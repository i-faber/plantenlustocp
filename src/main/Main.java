package main;

import factory.DataEditorCreator;
import factory.DatabaseMediatorCreator;
import factory.MenuMediatorCreator;
import view.ModelStyle;
import userInterface.Navigation;
import userInterface.UIMediator;
import userInterface.menu.MainMenu;
import utility.Finder;
import view.ModelViewer;

public class Main {
    private final String DB_NAME = "plantenlust.db";
    private final String DB_LOCATION = "jdbc:sqlite:C:\\cursus\\Java\\IntJ\\casus3_plantenlust_01\\";

    public static void main(String[] args) {
        new Main().new Initializer().init();
    }

    private class Initializer {

        private void init() {
            UIMediator uiMediator = new UIMediator();
            uiMediator.registerComponent(DatabaseMediatorCreator.createForSQL(DB_LOCATION, DB_NAME));
            uiMediator.registerComponent(DataEditorCreator.create());
            uiMediator.registerComponent(new Finder());
            uiMediator.registerComponent(new Navigation());
            uiMediator.registerComponent(new ModelViewer(new ModelStyle()));
            uiMediator.registerComponent(MenuMediatorCreator.createForModelData());

            uiMediator.loadMenu(MainMenu.class);
        }
    }
}

