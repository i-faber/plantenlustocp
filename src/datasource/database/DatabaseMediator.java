package datasource.database;

import components.Component;
import components.TwoWayComponent;
import factory.DataObjectCreator;
import mediators.Mediator;
import model.DataObject;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

public abstract class DatabaseMediator implements Component, Mediator {
    private DataObjectCreator dataObjectCreator;

    final Map<Class<? extends DataObject>, DatabaseTable> tables = new HashMap<>();

    final String dbLocation;
    final String dbName;

    public DatabaseMediator(String dbLocation, String dbName) {
        this.dbLocation = dbLocation;
        this.dbName = dbName;
    }

    // Getters
    public abstract Connection getConn();
    public abstract DatabaseTable getTable(Class<? extends DataObject> type);

    public DataObjectCreator getDataObjectCreator() {
        return dataObjectCreator;
    }

    // Methods
    public abstract boolean connect();
    public abstract boolean close();

    public void registerTable(DatabaseTable table) {
        table.setMediator(this);
        tables.put(table.getType(), table);
    }

    @Override
    public void registerComponent(Component component) {
        if (component instanceof TwoWayComponent) {
            ((TwoWayComponent) component).setMediator(this);
        }

        if (component instanceof DataObjectCreator) {
            this.dataObjectCreator = (DataObjectCreator) component;
        }
    }
}
