package datasource.database.dbTables;

import datasource.database.DatabaseTable;
import model.Order;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.SortedMap;
import java.util.TreeMap;

public class OrderTable extends DatabaseTable {
    private static final String TABLE_NAME = "bestellingen";
    private static final String COLUMN_ORDER_NR = "bestelnr";
    private static final String COLUMN_SUPPLIER_CODE = "levcode";
    private static final String COLUMN_ORDER_DATE = "besteldat";
    private static final String COLUMN_DELIVERY_DATE = "leverdat";
    private static final String COLUMN_AMOUNT = "bedrag";
    private static final String COLUMN_STATUS = "status";

    // QUERY
    private static final String QUERY_ORDERS_BY_SUPPLIER_PREP = "SELECT * FROM " + TABLE_NAME +
            " WHERE " + COLUMN_SUPPLIER_CODE + " = ?";
    private static final String QUERY_ORDER_DATE_BY_ORDER_NR_PREP =
            "SELECT " + COLUMN_ORDER_DATE + " FROM " + TABLE_NAME +
            " WHERE " + COLUMN_ORDER_NR + " = ?";
    private static final String QUERY_ORDER_BY_SUPPLIER_CODE_ORDER_NR = "SELECT * FROM " + TABLE_NAME +
            " WHERE " + COLUMN_SUPPLIER_CODE + " = ? AND " + COLUMN_ORDER_NR + " = ?";

    // UPDATES
    private static final String UPDATE_STATUS_BY_ORDER_NR = "UPDATE " + TABLE_NAME +
            " SET " + COLUMN_STATUS + " = ?" +
            " WHERE " + COLUMN_ORDER_NR + " = ?";
    private static final String UPDATE_DELIVERY_DATE_BY_ORDER_NR = "UPDATE " + TABLE_NAME +
                    " SET " + COLUMN_DELIVERY_DATE + " = ?" +
                    " WHERE " + COLUMN_ORDER_NR + " = ?";

    // PREPARED STATEMENTS
    private PreparedStatement queryOrdersBySupplierPrep;
    private PreparedStatement queryOrderDateByOrderNrPrep;
    private PreparedStatement queryOrderBySupplierOrderNr;
    private PreparedStatement updateStatusOrderPrep;
    private PreparedStatement updateDeliveryDatePrep;

    private ResultSet results;

    public OrderTable() {
        super(Order.class);
    }

    @Override
    protected boolean closeResources() {
        try {
            if (results != null) {
                results.close();
            }
            if (queryOrdersBySupplierPrep != null) {
                queryOrdersBySupplierPrep.close();
            }
            if (queryOrderDateByOrderNrPrep != null) {
                queryOrderDateByOrderNrPrep.close();
            }
            if (queryOrderBySupplierOrderNr != null) {
                queryOrderBySupplierOrderNr.close();
            }
            if (updateStatusOrderPrep != null) {
                updateStatusOrderPrep.close();
            }
            if (updateDeliveryDatePrep != null) {
                updateDeliveryDatePrep.close();
            }
            return true;
        } catch (SQLException e) {
            System.out.println("Problem closing resources: " + e.getMessage());
            return false;
        }
    }

    private SortedMap<String, Order> fillMap(ResultSet results) throws SQLException {
        SortedMap<String, Order> orders = new TreeMap<>(stringKeyComparator);
        while (results.next()) {
            try {
                String key = "" + results.getInt(COLUMN_ORDER_NR);
                orders.put(key, createOrder(results));
            } catch (Exception e) {
                System.out.println("Problem creating Order: " + e.getMessage());
            }
        }
        return orders;
    }

    private Order createOrder(ResultSet results) throws SQLException {
        return getDatabaseMediator().getDataObjectCreator().create(
                results.getInt(COLUMN_ORDER_NR),
                results.getInt(COLUMN_SUPPLIER_CODE),
                results.getString(COLUMN_ORDER_DATE),
                results.getString(COLUMN_DELIVERY_DATE),
                results.getDouble(COLUMN_AMOUNT),
                results.getString(COLUMN_STATUS)
        );
    }

    public SortedMap<String, Order> fetchTable() {
        try (Statement statement = getDatabaseMediator().getConn().createStatement();
             ResultSet results = statement.executeQuery("SELECT * FROM " + TABLE_NAME)) {

            SortedMap<String, Order> orders = fillMap(results);
            return (orders.size() > 0 ? orders : null);

        } catch (SQLException e) {
            System.out.println("Problem retrieving data from table: " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    public SortedMap<String, Order> findOrdersBySupplier(int supplierCode) {
        try {
            queryOrdersBySupplierPrep = getDatabaseMediator().getConn().prepareStatement(QUERY_ORDERS_BY_SUPPLIER_PREP);
            queryOrdersBySupplierPrep.setInt(1, supplierCode);
            results = queryOrdersBySupplierPrep.executeQuery();

            SortedMap<String, Order> orders = fillMap(results);
            return (orders.size() > 0 ? orders : null);

        } catch (SQLException e) {
            System.out.println("Problem retrieving data from table: " + e.getMessage());
            e.printStackTrace();
            return null;
        } finally {
            closeResources();
        }
    }

    public Order findOrderBySupplierAndOrderNr(int supplierCode, int orderNr) {
        try {
            queryOrderBySupplierOrderNr = getDatabaseMediator().getConn().prepareStatement(QUERY_ORDER_BY_SUPPLIER_CODE_ORDER_NR);
            queryOrderBySupplierOrderNr.setInt(1, supplierCode);
            queryOrderBySupplierOrderNr.setInt(2, orderNr);
            results = queryOrderBySupplierOrderNr.executeQuery();

            return (results.next() ? createOrder(results) : null);

        } catch (SQLException e) {
            System.out.println("Problem retrieving data from table: " + e.getMessage());
            e.printStackTrace();
            return null;
        } finally {
            closeResources();
        }

    }

    public boolean updateOrderStatus(String status, int orderNr) {
        try {
            updateStatusOrderPrep = getDatabaseMediator().getConn().prepareStatement(UPDATE_STATUS_BY_ORDER_NR);
            updateStatusOrderPrep.setString(1, status.toUpperCase());
            updateStatusOrderPrep.setInt(2, orderNr);
            updateStatusOrderPrep.executeUpdate();
            return true;

        } catch (SQLException e) {
            System.out.println("Problem updating order status: " + e.getMessage());
            e.printStackTrace();
            return false;
        } finally {
            closeResources();
        }
    }

    public boolean updateDeliveryDate(String deliveryDate, int orderNr) {
        try {
            updateDeliveryDatePrep = getDatabaseMediator().getConn().prepareStatement(UPDATE_DELIVERY_DATE_BY_ORDER_NR);
            updateDeliveryDatePrep.setString(1, deliveryDate);
            updateDeliveryDatePrep.setInt(2, orderNr);
            updateDeliveryDatePrep.executeUpdate();
            return true;

        } catch (SQLException e) {
            System.out.println("Problem updating delivery date: " + e.getMessage());
            e.printStackTrace();
            return false;
        } finally {
            closeResources();
        }
    }

    public String fetchOrderDateByOrderNr(int orderNr) {
            try {
            queryOrderDateByOrderNrPrep = getDatabaseMediator().getConn().prepareStatement(QUERY_ORDER_DATE_BY_ORDER_NR_PREP);
            queryOrderDateByOrderNrPrep.setInt(1, orderNr);
            results = queryOrderDateByOrderNrPrep.executeQuery();

            return results.getString(COLUMN_ORDER_DATE);

        } catch (SQLException e) {
            System.out.println("Problem finding order date: " + e.getMessage());
            e.printStackTrace();
            return null;
        } finally {
            closeResources();
        }
    }
}