package datasource.database.dbTables;

import datasource.database.DatabaseTable;
import model.GoodsReceipt;
import model.ModelData;
import reference.GlobalSymbols;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.SortedMap;
import java.util.TreeMap;

public class GoodsReceiptTable extends DatabaseTable {
    private static final String TABLE_NAME = "goed_ontvangst";
    private static final String COLUMN_ORDER_NR = "bestelnr";
    private static final String COLUMN_ITEM_CODE = "artcode";
    private static final String COLUMN_DATE_OF_RECEIPT = "ontv_datum";
    private static final String COLUMN_RECEIVED_QUANTITY = "ontv_aantal";
    private static final String COLUMN_STATUS = "ontv_status";
    private static final String COLUMN_ENTRY_CODE = "boekstuk";
    private static final String COLUMN_SERIAL_NUM = "volgnr";

    // QUERIES
    private static final String QUERY_ORDER_NR_PREP = "SELECT * FROM " + TABLE_NAME +
            " WHERE " + COLUMN_ORDER_NR + " = ?";
    private static final String QUERY_ORDER_NR_ITEM_CODE_PREP = "SELECT * FROM " + TABLE_NAME +
            " WHERE " + COLUMN_ORDER_NR + " = ?" + " AND " + COLUMN_ITEM_CODE + " = ?";

    // PREPARED STATEMENTS
    private PreparedStatement queryOrderNr;
    private PreparedStatement queryOrderNrItemCode;

    private ResultSet results;

    public GoodsReceiptTable() {
        super(GoodsReceipt.class);
    }

    @Override
    protected boolean closeResources() {
        try {
            if (results != null) {
                results.close();
            }
            if (queryOrderNr != null) {
                queryOrderNr.close();
            }
            if (queryOrderNrItemCode != null) {
                queryOrderNrItemCode.close();
            }
            return true;
        } catch (SQLException e) {
            System.out.println("Problem closing resources: " + e.getMessage());
            return false;
        }
    }

    private SortedMap<String, GoodsReceipt> fillMap(ResultSet results) throws SQLException {
        SortedMap<String, GoodsReceipt> goodsReceipts = new TreeMap<>(stringKeyComparator);
        while (results.next()) {
            try {
                String key = "" + results.getInt(COLUMN_ORDER_NR) + GlobalSymbols.DELIMITER +
                        results.getInt(COLUMN_ITEM_CODE);
                goodsReceipts.put(key, getDatabaseMediator().getDataObjectCreator().create(
                        results.getInt(COLUMN_ORDER_NR),
                        results.getInt(COLUMN_ITEM_CODE),
                        results.getString(COLUMN_DATE_OF_RECEIPT),
                        results.getInt(COLUMN_RECEIVED_QUANTITY),
                        results.getString(COLUMN_STATUS),
                        results.getInt(COLUMN_ENTRY_CODE),
                        results.getInt(COLUMN_SERIAL_NUM)
                ));
            } catch (Exception e) {
                System.out.println("Problem creating Goods Receipt: " + e.getMessage());
            }
        }
        return goodsReceipts;
    }

    public SortedMap<String, GoodsReceipt> fetchTable() {
        try (Statement statement = getDatabaseMediator().getConn().createStatement();
             ResultSet results = statement.executeQuery("SELECT * FROM " + TABLE_NAME)) {

            SortedMap<String, GoodsReceipt> goodsReceipts = fillMap(results);
            return (goodsReceipts.size() > 0 ? goodsReceipts : null);
        } catch (SQLException e) {
            System.out.println("Problem retrieving data from table: " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    public SortedMap<String, GoodsReceipt> findReceiptsByOrderNr(int orderNr) {
        try {
            queryOrderNr = getDatabaseMediator().getConn().prepareStatement(QUERY_ORDER_NR_PREP);
            queryOrderNr.setInt(1, orderNr);
            results = queryOrderNr.executeQuery();

            SortedMap<String, GoodsReceipt> goodsReceipts = fillMap(results);
            return (goodsReceipts.size() > 0 ? goodsReceipts : null);

        } catch (SQLException e) {
            System.out.println("Problem retrieving data from table: " + e.getMessage());
            e.printStackTrace();
            return null;
        } finally {
            closeResources();
        }
    }

    public SortedMap<String,? extends ModelData> findReceiptsByPrimaryKey(int orderNr, int itemCode) {
        try {
            queryOrderNrItemCode = getDatabaseMediator().getConn().prepareStatement(QUERY_ORDER_NR_ITEM_CODE_PREP);
            queryOrderNrItemCode.setInt(1, orderNr);
            queryOrderNrItemCode.setInt(2, itemCode);
            results = queryOrderNrItemCode.executeQuery();

            SortedMap<String, GoodsReceipt> goodsReceipts = fillMap(results);
            return (goodsReceipts.size() > 0 ? goodsReceipts : null);

        } catch (SQLException e) {
            System.out.println("Problem retrieving data from table: " + e.getMessage());
            e.printStackTrace();
            return null;
        } finally {
            closeResources();
        }
    }
}