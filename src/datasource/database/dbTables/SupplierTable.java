package datasource.database.dbTables;

import datasource.database.DatabaseTable;
import model.Supplier;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.SortedMap;
import java.util.TreeMap;

public class SupplierTable extends DatabaseTable {
    private static final String TABLE_NAME = "leveranciers";
    private static final String COLUMN_SUPPLIER_CODE = "levcode";
    private static final String COLUMN_NAME = "naam";
    private static final String COLUMN_ADDRESS = "adres";
    private static final String COLUMN_LOCATION = "woonplaats";

    // QUERIES
    private static final String QUERY_SUPPLIER_CODE_PREP = "SELECT * FROM " + TABLE_NAME +
            " WHERE " + COLUMN_SUPPLIER_CODE + " = ?";

    // PREPARED STATEMENTS
    private PreparedStatement querySupplierCode;

    private ResultSet results;

    public SupplierTable() {
        super(Supplier.class);
    }

    @Override
    protected boolean closeResources() {
        try {
            if (results != null) {
                results.close();
            }
            if (querySupplierCode != null) {
                querySupplierCode.close();
            }
            return true;
        } catch (SQLException e) {
            System.out.println("Problem closing resources: " + e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

    private SortedMap<String, Supplier> fillMap(ResultSet results) throws SQLException{
        SortedMap<String, Supplier> suppliers = new TreeMap<>(stringKeyComparator);

        while (results.next()) {
            try {
                String key = "" + results.getInt(COLUMN_SUPPLIER_CODE);
                suppliers.put(key, getDatabaseMediator().getDataObjectCreator().create(
                        results.getInt(COLUMN_SUPPLIER_CODE),
                        results.getString(COLUMN_NAME),
                        results.getString(COLUMN_ADDRESS),
                        results.getString(COLUMN_LOCATION)));
            } catch (Exception e) {
                System.out.println("Problem creating Supplier: " + e.getMessage());
            }
        }
        return suppliers;
    }

    @Override
    public SortedMap<String, Supplier> fetchTable() {
        try (Statement statement = getDatabaseMediator().getConn().createStatement();
             ResultSet results = statement.executeQuery("SELECT * FROM " + TABLE_NAME)) {

            SortedMap<String, Supplier> suppliers = fillMap(results);
            return (suppliers.size() > 0 ? suppliers : null);

        } catch (SQLException e) {
            System.out.println("Problem retrieving data from table: " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    public SortedMap<String, Supplier> findSupplierCode(int supplierCode) {
        try {
            querySupplierCode = getDatabaseMediator().getConn().prepareStatement(QUERY_SUPPLIER_CODE_PREP);
            querySupplierCode.setInt(1, supplierCode);
            results = querySupplierCode.executeQuery();

            SortedMap<String, Supplier> suppliers = fillMap(results);
            return (suppliers.size() > 0 ? suppliers : null);

        } catch (SQLException e) {
            System.out.println("Problem retrieving data from table: " + e.getMessage());
            e.printStackTrace();
            return null;
        } finally {
            closeResources();
        }
    }
}
