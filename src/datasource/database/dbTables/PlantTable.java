package datasource.database.dbTables;

import datasource.database.DatabaseTable;
import model.Plant;

import java.sql.*;
import java.util.SortedMap;
import java.util.TreeMap;

public class PlantTable extends DatabaseTable {
    private static final String TABLE_NAME = "planten";
    private static final String COLUMN_ITEM_CODE = "artcode";
    private static final String COLUMN_NAME = "plantennaam";
    private static final String COLUMN_SPECIES = "soort";
    private static final String COLUMN_COLOUR = "kleur";
    private static final String COLUMN_HEIGHT = "hoogte";
    private static final String COLUMN_FLOWERING_START = "bloeibeg";
    private static final String COLUMN_FLOWERING_END = "bloeieind";
    private static final String COLUMN_PRICE = "prijs";
    private static final String COLUMN_STOCK_QUANTITY = "vrr_aantal";
    private static final String COLUMN_STOCK_MIN = "vrr_min";

    // QUERIES
    private static final String QUERY_ITEM_CODE_PREP = "SELECT * FROM " + TABLE_NAME +
            " WHERE " + COLUMN_ITEM_CODE + " = ?";
    private static final String FIND_NAME_BY_ITEM_CODE_PREP = "SELECT " + COLUMN_NAME + " FROM " + TABLE_NAME +
            " WHERE " + COLUMN_ITEM_CODE + " = ?";

    // PREPARED STATEMENTS
    private PreparedStatement queryItemCode;
    private PreparedStatement queryNameByItemCode;

    private ResultSet results;

    public PlantTable() {
        super(Plant.class);
    }

    @Override
    protected boolean closeResources() {
        try {
            if (results != null) {
                results.close();
            }
            if (queryItemCode != null) {
                queryItemCode.close();
            }
            if (queryNameByItemCode != null) {
                queryNameByItemCode.close();
            }
            return true;
        } catch (SQLException e) {
            System.out.println("Problem closing resources: " + e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

    private SortedMap<String, Plant> fillMap(ResultSet results) throws SQLException{
        SortedMap<String, Plant> plants = new TreeMap<>(stringKeyComparator);
        while (results.next()) {
            try {
                String key = "" + results.getInt(COLUMN_ITEM_CODE);
                plants.put(key, getDatabaseMediator().getDataObjectCreator().create(
                        results.getInt(COLUMN_ITEM_CODE),
                        results.getString(COLUMN_NAME),
                        results.getString(COLUMN_SPECIES),
                        results.getString(COLUMN_COLOUR),
                        results.getInt(COLUMN_HEIGHT),
                        results.getInt(COLUMN_FLOWERING_START),
                        results.getInt(COLUMN_FLOWERING_END),
                        results.getDouble(COLUMN_PRICE),
                        results.getInt(COLUMN_STOCK_QUANTITY),
                        results.getInt(COLUMN_STOCK_MIN)
                ));
            } catch (Exception e) {
                System.out.println("Problem creating Plant: " + e.getMessage());
            }
        }
        return plants;
    }

    public SortedMap<String, Plant> fetchTable() {
        try (Statement statement = getDatabaseMediator().getConn().createStatement();
             ResultSet results = statement.executeQuery("SELECT * FROM " + TABLE_NAME)) {

            SortedMap<String, Plant> plants = fillMap(results);
            return (plants.size() > 0 ? plants : null);

        } catch (SQLException e) {
            System.out.println("Problem retrieving data from table: " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    public SortedMap<String, Plant> findPlantByItemCode(int itemCode) {
        try {
            queryItemCode = getDatabaseMediator().getConn().prepareStatement(QUERY_ITEM_CODE_PREP);
            queryItemCode.setInt(1, itemCode);
            results = queryItemCode.executeQuery();

            SortedMap<String, Plant> plants = fillMap(results);
            return (plants.size() > 0 ? plants : null);

        } catch (SQLException e) {
            System.out.println("Problem retrieving data from table: " + e.getMessage());
            e.printStackTrace();
            return null;
        } finally {
            closeResources();
        }
    }

    public String findPlantNameByItemCode(int itemCode) {
        try {
            queryNameByItemCode = getDatabaseMediator().getConn().prepareStatement(FIND_NAME_BY_ITEM_CODE_PREP);
            queryNameByItemCode.setInt(1, itemCode);
            results = queryNameByItemCode.executeQuery();

            return results.getString(COLUMN_NAME);

        } catch (SQLException e) {
            System.out.println("Problem retrieving data from table: " + e.getMessage());
            e.printStackTrace();
            return "";
        } finally {
            closeResources();
        }
    }
}
