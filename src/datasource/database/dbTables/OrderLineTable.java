package datasource.database.dbTables;

import datasource.database.DatabaseTable;
import model.OrderLine;
import reference.GlobalSymbols;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.SortedMap;
import java.util.TreeMap;

public class OrderLineTable extends DatabaseTable {
    private static final String TABLE_NAME = "bestelregels";
    private static final String COLUMN_ORDER_NR = "bestelnr";
    private static final String COLUMN_ITEM_CODE = "artcode";
    private static final String COLUMN_QUANTITY = "aantal";
    private static final String COLUMN_PRICE = "bestelprijs";

    // QUERIES
    private static final String QUERY_ORDER_NR_PREP = "SELECT * FROM " + TABLE_NAME +
            " WHERE " + COLUMN_ORDER_NR + " = ?";

    // PREPARED STATEMENTS
    private PreparedStatement queryOrderNr;

    private ResultSet results;

    public OrderLineTable() {
        super(OrderLine.class);
    }

    @Override
    protected boolean closeResources() {
        try {
            if (results != null) {
                results.close();
            }
            if (queryOrderNr != null) {
                queryOrderNr.close();
            }
            return true;
        } catch (SQLException e) {
            System.out.println("Problem closing resources: " + e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

    private SortedMap<String, OrderLine> fillMap(ResultSet results) throws SQLException{
        SortedMap<String, OrderLine> orderLines = new TreeMap<>(stringKeyComparator);
        while (results.next()) {
            try {
                String key = "" + results.getInt(COLUMN_ORDER_NR) + GlobalSymbols.DELIMITER + results.getInt(COLUMN_ITEM_CODE);
                orderLines.put(key, getDatabaseMediator().getDataObjectCreator().create(
                        results.getInt(COLUMN_ORDER_NR),
                        results.getInt(COLUMN_ITEM_CODE),
                        results.getInt(COLUMN_QUANTITY),
                        results.getDouble(COLUMN_PRICE)
                ));
            } catch (Exception e) {
                System.out.println("Problem creating Order Line: " + e.getMessage());
            }
        }
        return orderLines;
    }

    public SortedMap<String, OrderLine> fetchTable() {
        try (Statement statement = getDatabaseMediator().getConn().createStatement();
             ResultSet results = statement.executeQuery("SELECT * FROM " + TABLE_NAME)) {

            SortedMap<String, OrderLine> orderLines = fillMap(results);
            return (orderLines.size() > 0 ? orderLines : null);

        } catch (SQLException e) {
            System.out.println("Problem retrieving data from table: " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    public SortedMap<String, OrderLine> findOrderLines(int orderNr) {
        try {
            queryOrderNr = getDatabaseMediator().getConn().prepareStatement(QUERY_ORDER_NR_PREP);
            queryOrderNr.setInt(1, orderNr);
            results = queryOrderNr.executeQuery();

            SortedMap<String, OrderLine> orderLines = fillMap(results);
            return (orderLines.size() > 0 ? orderLines : null);

        } catch (SQLException e) {
            System.out.println("Problem retrieving data from table: " + e.getMessage());
            e.printStackTrace();
            return null;
        } finally {
            closeResources();
        }
    }
}
