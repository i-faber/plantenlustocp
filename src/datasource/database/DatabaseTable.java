package datasource.database;

import model.ModelData;

import java.util.Comparator;
import java.util.SortedMap;

public abstract class DatabaseTable {
    private final Class type;
    private DatabaseMediator databaseMediator;

    protected DatabaseTable(Class type) {
        this.type = type;
    }

    public Class getType() {
        return type;
    }

    protected DatabaseMediator getDatabaseMediator() {
        return databaseMediator;
    }

    public void setMediator(DatabaseMediator databaseMediator) {
        this.databaseMediator = databaseMediator;
    }

    protected abstract boolean closeResources();

    public abstract SortedMap<String, ? extends ModelData> fetchTable();

    protected final Comparator<String> stringKeyComparator = (key1, key2) -> {
        if (key1.length() < key2.length()) {
            return -1;
        } else if (key1.length() > key2.length()) {
            return 1;
        }
        return key1.compareTo(key2);
    };
}
