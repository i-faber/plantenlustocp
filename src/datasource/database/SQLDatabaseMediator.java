package datasource.database;

import model.DataObject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SQLDatabaseMediator extends DatabaseMediator {
    private Connection conn;

    public SQLDatabaseMediator(String dbLocation, String dbName) {
        super(dbLocation, dbName);
    }

    // Getter
    @Override
    public Connection getConn() {
        if (conn == null) {
            connect();
        }
        return conn;
    }

    @Override
    public DatabaseTable getTable(Class<? extends DataObject> type) {
        return tables.get(type);
    }

    // Methods
    @Override
    public boolean connect() {
        try {
            conn = DriverManager.getConnection(dbLocation + dbName);
            return true;
        } catch (SQLException e) {
            System.out.println("Problem connecting to database " + dbName + ": " + e.getMessage());
            e.getStackTrace();
            return false;
        }
    }

    @Override
    public boolean close() {
        try {
            if(conn != null) {
                conn.close();
            }
            return true;
        } catch (SQLException e) {
            System.out.println("Problem closing connection with database " + dbName + ": " + e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

}
