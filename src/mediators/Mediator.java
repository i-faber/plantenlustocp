package mediators;

import components.Component;

public interface Mediator {
    void registerComponent(Component component);
}
