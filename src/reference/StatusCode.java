package reference;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class StatusCode {
    private static final List<String> ORDER_CODES = Arrays.asList("A", "B", "C", "E", "J", "R");

    public static List<String> getOrderCodes() {
        return Collections.unmodifiableList(ORDER_CODES);
    }
}
