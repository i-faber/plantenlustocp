import datasource.database.DatabaseMediator;
import factory.DatabaseMediatorCreator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import utility.InputReader;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;

public class SupplierMenuTest {

    private final InputStream systemIn = System.in;
    private ByteArrayInputStream testIn;

    DatabaseMediator db;

    @Before
    public void init() {
        db = DatabaseMediatorCreator.createForSQL("jdbc:sqlite:C:\\cursus\\Java\\IntJ\\casus3_plantenlust_01\\", "plantenlust.db");
    }

    public void provideInput(String data) {
        testIn = new ByteArrayInputStream(data.getBytes());
        System.setIn(testIn);
    }

    @After
    public void tearDown() {
        db.close();
        System.setIn(systemIn);
    }

    @Test
    public void testViewOrderList() {
        String data = "4";
        provideInput(data);

//        new SupplierMenu().viewOrderList();
    }

    @Test
    public void testReadInt() {
        String data = "4";
        provideInput(data);

        int choice = new InputReader().readInt("Insert number here: ");
        System.out.println(choice);
        assertEquals(data, String.valueOf(choice));
    }

    @Test
    public void testReadString() {
        String data = "Hello, World!";
        provideInput(data);

        String text = new InputReader().readString("Insert text here: ");
        System.out.println(text);
        assertEquals(data, text);
    }

//    @Test
//    public void testScanner() {
//        String data = "Hello, World!\r\n";
//        InputStream stdin = System.in;
//        try {
//            System.setIn(new ByteArrayInputStream(data.getBytes()));
//            Scanner scanner = new Scanner(System.in);
//            System.out.println(scanner.nextLine());
//        } finally {
//            System.setIn(stdin);
//        }
//    }
}
