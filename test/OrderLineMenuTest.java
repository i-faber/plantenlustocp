import datasource.database.DatabaseMediator;
import datasource.database.SQLDatabaseMediator;
import org.junit.After;
import org.junit.Before;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class OrderLineMenuTest {
    private final InputStream systemIn = System.in;
    private ByteArrayInputStream testIn;

    DatabaseMediator db;

    @Before
    public void init() {
        db = new SQLDatabaseMediator("jdbc:sqlite:C:\\cursus\\Java\\IntJ\\casus3_plantenlust_01\\", "plantenlust.db");
        db.connect();
    }

    public void provideInput(String data) {
        testIn = new ByteArrayInputStream(data.getBytes());
        System.setIn(testIn);
    }

    @After
    public void tearDown() {
        db.close();
        System.setIn(systemIn);
    }

//    @Test
//    public void testLoadRequestedData() {
//        int orderNr = 174;
//        SortedMap<String, ? extends ModelData> map = OrderLineTable.findOrderLines(orderNr);
//        for (Map.Entry<String, ? extends ModelData> entry : map.entrySet()) {
//            System.out.println(entry);
//        }
//
//        Navigation.setCurrentRenderedMap(map);
//        DbTableHandler.setLastUsedKey(String.valueOf(orderNr));
//        String data = String.valueOf(102);
//        provideInput(data);
//
//        OrderLineMenu menu = new OrderLineMenu();
//        menu.loadRequestedData(ModelDataType.GOODS_RECEIPT, menu.GOODS_MESSAGES);
//    }
}
