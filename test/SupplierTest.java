import model.Order;
import model.Supplier;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;

public class SupplierTest {
    List<Supplier> suppliers;
    List<Order> orders;

    @Before
    public void init() {
        suppliers = new ArrayList<>();
        suppliers.add(new
                Supplier(4, "HOVENIER G.H.", "ZANDWEG 50", "LISSE"));
        suppliers.add(new
                Supplier(13, "STRUIK BV", "BESSENLAAN 1", "LISSE"));
        suppliers.add(new
                Supplier(9, "BAUMGARTEN R.", "TAKSTRAAT 13", "HILLEGOM"));

        orders = new ArrayList<>();
        orders.add(new Order(174, 4, "25-8-2015", "4-9-2015", 117.5d, "C"));
   		orders.add(new Order(121, 13, "17-7-2015", "31-7-2015", 602.5d, "C"));
		orders.add(new Order(181, 9, "6-9-2015", "27-9-2015", 607.6d, "C"));
    }

    @Test
    public void testSortSuppliersStream() {
        List<Supplier> sortedList = suppliers.stream()
                .sorted(comparing(e -> e.getSupplierCode()))
                .collect(Collectors.toList());
        for (Supplier supplier : sortedList) {
            System.out.println(supplier.getName() + " " + supplier.getSupplierCode());
        }
    }

    @Test
    public void testFindSupplierBinary() {
        Supplier test = new Supplier(13, "Tester", "Test street", "Test ville");
        int match = Collections.binarySearch(suppliers, test);
        System.out.println(match);
    }

    @Test
    public void testSortSupplierCollections() {
        System.out.println("Before sorting");
        for (Supplier lev : suppliers) {
            System.out.println(Arrays.deepToString(lev.getData()));
        }
        Collections.sort(suppliers);

        System.out.println("After sorting");
        for (Supplier lev : suppliers) {
            System.out.println(Arrays.deepToString(lev.getData()));
        }
    }
}
