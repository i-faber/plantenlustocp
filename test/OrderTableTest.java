import datasource.database.DatabaseMediator;
import datasource.database.SQLDatabaseMediator;
import datasource.database.dbTables.OrderTable;
import factory.DataObjectCreator;
import model.Order;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import view.ModelStyle;
import view.ModelViewer;

import java.util.SortedMap;

import static org.junit.Assert.assertEquals;

public class OrderTableTest {
    DatabaseMediator db;

    @Before
    public void init() {
        db = new SQLDatabaseMediator("jdbc:sqlite:C:\\cursus\\Java\\IntJ\\casus3_plantenlust_01\\", "plantenlust.db");
        db.connect();
    }

    @After
    public void tearDown() {
        db.close();
    }

    @Test
    public void testFetchTable() {
        OrderTable table = new OrderTable();
        db.registerTable(table);
        db.registerComponent(new DataObjectCreator());
        SortedMap<String, Order> orders = table.fetchTable();
        new ModelViewer(new ModelStyle()).viewModel(orders);
    }

    @Test
    public void testFinderOrderBySupplierOrderNr() {
        OrderTable table = new OrderTable();
        db.registerTable(table);
        db.registerComponent(new DataObjectCreator());
        Order order = table.findOrderBySupplierAndOrderNr(4, 174);
        System.out.println(order);
    }

    @Test
    public void testFindOrdersBySuppliers() {
        SortedMap<String, Order> orders = new OrderTable().findOrdersBySupplier(4);
//        for (Map.Entry<String, Order> order : orders.entrySet()) {
//            System.out.println(order.getValue());
//        }
        new ModelViewer(new ModelStyle()).viewModel(orders);
//        assertTrue(orders.containsKey("121"));
    }

    @Test
    public void testUpdateStatusByOrderNr() {
        boolean result = new OrderTable().updateOrderStatus("C", 121);
        assertEquals(true, result);
    }

    @Test
    public void testFetchOrderDate() {
        System.out.println(new OrderTable().fetchOrderDateByOrderNr(121));
    }

    @Test
    public void testUpdateDeliveryDateByOrderNr() {
        boolean result = new OrderTable().updateDeliveryDate("31-07-1997", 121);
        System.out.println(result);
        assertEquals(true, result);
    }

}
