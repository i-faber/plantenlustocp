import datasource.database.DatabaseMediator;
import datasource.database.SQLDatabaseMediator;
import datasource.database.dbTables.OrderLineTable;
import model.OrderLine;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import view.ModelStyle;
import view.ModelViewer;

import java.util.SortedMap;

public class OrderLineTableTest {
    DatabaseMediator db;

    @Before
    public void init() {
        db = new SQLDatabaseMediator("jdbc:sqlite:C:\\cursus\\Java\\IntJ\\casus3_plantenlust_01\\", "plantenlust.db");
        db.connect();
    }

    @After
    public void tearDown() {
        db.close();
    }

    @Test
    public void testFetchTable() {
        OrderLineTable orderLineTable = new OrderLineTable();
        SortedMap<String, OrderLine> orderLines = orderLineTable.fetchTable();
        new ModelViewer(new ModelStyle()).viewModel(orderLines);
    }

    @Test
    public void testFetchOrderLines() {
        OrderLineTable table = new OrderLineTable();
        SortedMap<String, OrderLine> orderLines = table.findOrderLines(121);
        new ModelViewer(new ModelStyle()).viewModel(orderLines);
    }
}
