import datasource.database.DatabaseMediator;
import datasource.database.SQLDatabaseMediator;
import datasource.database.dbTables.GoodsReceiptTable;
import model.GoodsReceipt;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import view.ModelStyle;
import view.ModelViewer;

import java.util.SortedMap;

public class GoodsReceiptTableTest {
    DatabaseMediator db;

    @Before
    public void init() {
        db = new SQLDatabaseMediator("jdbc:sqlite:C:\\cursus\\Java\\IntJ\\casus3_plantenlust_01\\", "plantenlust.db");
        db.connect();
    }

    @After
    public void tearDown() {
        db.close();
    }

    @Test
    public void testFetchTable() {
        SortedMap<String, GoodsReceipt> result = new GoodsReceiptTable().fetchTable();

//        for (Map.Entry<String, GoodsReceipt> entry : result.entrySet()) {
//            System.out.println(entry.getKey() + " " + entry.getValue());
//        }
        new ModelViewer(new ModelStyle()).viewModel(result);

    }

    @Test
    public void testFindReceiptsByOrderNr() {
        SortedMap<String, GoodsReceipt> results = new GoodsReceiptTable().findReceiptsByOrderNr(121);
        new ModelViewer(new ModelStyle()).viewModel(results);
    }

    @Test
    public void testFindReceiptsByOrderNrAndItemCode() {
        new ModelViewer(new ModelStyle()).viewModel(new GoodsReceiptTable().findReceiptsByPrimaryKey(121, 31));
    }
}
