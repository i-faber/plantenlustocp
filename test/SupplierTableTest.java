import datasource.database.DatabaseMediator;
import datasource.database.SQLDatabaseMediator;
import datasource.database.dbTables.SupplierTable;
import factory.DataObjectCreator;
import model.Supplier;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import view.ModelStyle;
import view.ModelViewer;

import java.util.Map;
import java.util.SortedMap;

public class SupplierTableTest {
    DatabaseMediator db;

    @Before
    public void init() {
        db = new SQLDatabaseMediator("jdbc:sqlite:C:\\cursus\\Java\\IntJ\\casus3_plantenlust_01\\", "plantenlust.db");
        db.connect();
    }

    @After
    public void tearDown() {
        db.close();
    }

    @Test
    public void testLoadSuppliersFromDB() {

        SupplierTable supplierTable = new SupplierTable();
        db.registerTable(supplierTable);
        db.registerComponent(new DataObjectCreator());
        SortedMap<String, Supplier> results = supplierTable.fetchTable();
        new ModelViewer(new ModelStyle()).viewModel(results);

    }

    @Test
    public void testSelectSupplier() {
        SupplierTable table = new SupplierTable();
        SortedMap<String, Supplier> suppliers = table.findSupplierCode(4);
        for (Map.Entry<String, Supplier> lev : suppliers.entrySet()) {
            System.out.println(lev);
        }
    }
}
