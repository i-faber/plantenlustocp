import datasource.database.DatabaseMediator;
import factory.DataEditorCreator;
import factory.DatabaseMediatorCreator;
import model.ModelData;
import model.Order;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import utility.DataEditorMediator;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.SortedMap;

import static org.junit.Assert.assertEquals;

public class OrderEditorTest {
    DatabaseMediator db;

    private final InputStream systemIn = System.in;
    private ByteArrayInputStream testIn;

    @Before
    public void init() {
        db = DatabaseMediatorCreator.createForSQL("jdbc:sqlite:C:\\cursus\\Java\\IntJ\\casus3_plantenlust_01\\", "plantenlust.db");
        db.connect();
    }

    public void provideInput(String data) {
        testIn = new ByteArrayInputStream(data.getBytes());
        System.setIn(testIn);
    }

    @After
    public void tearDown() {
        db.close();
        System.setIn(systemIn);
    }

    @Test
    public void testEditDeliveryDate() {
        String data = "1-1-2003";
        provideInput(data);

        DataEditorMediator editor = DataEditorCreator.create();
        SortedMap<String, ? extends ModelData> map = db.getTable(Order.class).fetchTable();
        int orderNr = 174;

        boolean successful = editor.editDeliveryDate(map, orderNr);
        System.out.println(successful);
        assertEquals(true, successful);
    }
}
