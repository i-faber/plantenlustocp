import datasource.database.DatabaseMediator;
import datasource.database.SQLDatabaseMediator;
import datasource.database.dbTables.OrderTable;
import model.ModelData;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import view.ModelStyle;
import view.ModelViewer;

import java.util.SortedMap;

public class ModelViewerTest {
    DatabaseMediator db;

    @Before
    public void init() {
        db = new SQLDatabaseMediator("jdbc:sqlite:C:\\cursus\\Java\\IntJ\\casus3_plantenlust_01\\", "plantenlust.db");
        db.connect();
    }

    @After
    public void tearDown() {
        db.close();
    }

    @Test
    public void testViewModelFetchMap() {
        SortedMap<String, ? extends ModelData> list = new OrderTable().fetchTable();
        new ModelViewer(new ModelStyle()).viewModel(list);
    }

    @Test
    public void testViewModelSelection() {
        SortedMap<String, ? extends ModelData> map = new OrderTable().findOrdersBySupplier(13);
        new ModelViewer(new ModelStyle()).viewModel(map);
    }
}
