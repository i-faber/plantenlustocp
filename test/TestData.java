import model.Supplier;

import java.util.ArrayList;
import java.util.List;

public class TestData {

	public static void loadTestData() {
		List<Supplier> suppliers = new ArrayList<>();

//		artikelen.add(new Plant(31, "WIJNSTOK", "BOOM", "", 600, 0, 0, 10d, 100, 50));
//		artikelen.add(new Plant(87, "KLOKJESBLOEM", "VAST", "BLAUW", 90, 6, 8, 3d, 100, 50));
//		artikelen.add(new Plant(311, "LEVERKRUID", "VAST", "PAARS", 175, 8, 9, 2.5d, 100, 50));
//		artikelen.add(new Plant(314, "CHRYSANT", "1-JARIG", "GEEL", 80, 6, 8, 0.8d, 100, 50));
//		artikelen.add(new Plant(102, "JUDASPENING", "2-JARIG", "LILA", 70, 5, 7, 1d, 100, 50));
//
		suppliers.add(new Supplier(4, "HOVENIER G.H.", "ZANDWEG 50", "LISSE"));
		suppliers.add(new Supplier(9, "BAUMGARTEN R.", "TAKSTRAAT 13", "HILLEGOM"));
		suppliers.add(new Supplier(13, "STRUIK BV", "BESSENLAAN 1", "LISSE"));
		
//		orders.add(new Order(121, 13, "17-7-2015", "31-7-2015", 602.5d, "C"));
//		orders.add(new Order(174, 4, "25-8-2015", "4-9-2015", 117.5d, "C"));
//		orders.add(new Order(181, 9, "6-9-2015", "27-9-2015", 607.6d, "C"));
//
//		orderregels.add(new OrderLine(121, 31, 25, 6.35d));
//		orderregels.add(new OrderLine(121, 87, 50, 1.9d));
//		orderregels.add(new OrderLine(121, 311, 50, 1.65d));
//		orderregels.add(new OrderLine(121, 314, 150, 0.45d));
//		orderregels.add(new OrderLine(174, 102, 25, 0.7d));
	}
}
