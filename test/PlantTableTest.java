import datasource.database.DatabaseMediator;
import datasource.database.SQLDatabaseMediator;
import datasource.database.dbTables.PlantTable;
import factory.DataObjectCreator;
import model.ModelData;
import model.Plant;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import view.ModelStyle;
import view.ModelViewer;

import java.util.Map;
import java.util.SortedMap;

public class PlantTableTest {
    DatabaseMediator db;

    @Before
    public void init() {
        db = new SQLDatabaseMediator("jdbc:sqlite:C:\\cursus\\Java\\IntJ\\casus3_plantenlust_01\\", "plantenlust.db");
        db.connect();
    }

    @After
    public void tearDown() {
        db.close();
    }

    @Test
    public void testShowTable() {
        PlantTable table = new PlantTable();
        db.registerTable(table);
        db.registerComponent(new DataObjectCreator());
        SortedMap<String, ? extends ModelData> results = table.fetchTable();
        new ModelViewer(new ModelStyle()).viewModel(results);
    }

    @Test
    public void testFindPlant() {
        PlantTable table = new PlantTable();
        SortedMap<String, Plant> plants = table.findPlantByItemCode(102);
        for (Map.Entry<String, Plant> plant : plants.entrySet()) {
            System.out.println(plant.toString());
        }
    }

    @Test
    public void testFindPlantName() {
        PlantTable table = new PlantTable();
        String name = table.findPlantNameByItemCode(74);
        System.out.println(name);
    }
}
