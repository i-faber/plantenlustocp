import datasource.database.DatabaseMediator;
import utility.Finder;
import factory.DatabaseMediatorCreator;
import model.GoodsReceipt;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class FinderTest {
    DatabaseMediator db;

    @Before
    public void init() {
        db = DatabaseMediatorCreator.createForSQL("jdbc:sqlite:C:\\cursus\\Java\\IntJ\\casus3_plantenlust_01\\", "plantenlust.db");
        db.connect();
    }

    @After
    public void tearDown() {
        db.close();
    }

    @Test
    public void testFindMatchingResults() {
        System.out.println(new Finder().findMatchingResults(GoodsReceipt.class, 102, 174));
    }

//    @Test
//    public void testFindMatch() {
//        SortedMap<String, Supplier> map = new TreeMap<>();
//        map.put("23", new Supplier(23, "Test", "Test location", "Test city"));
//
//        ModelData match = DbTableHandler.findMatch(map, 20);
//        if (match != null) {
//            System.out.println(Arrays.deepToString(match.getData()));
//        } else {
//            System.out.println("no match");
//        }
//    }
}
