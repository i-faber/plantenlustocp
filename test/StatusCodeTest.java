import org.junit.Test;
import reference.StatusCode;

import java.util.Comparator;

import static org.junit.Assert.assertTrue;

public class StatusCodeTest {
    @Test
    public void testStatusCode() {
        String status = "A";
        assertTrue(StatusCode.getOrderCodes().contains(status));
    }

    @Test (expected = UnsupportedOperationException.class)
    public void changeFinalList() {
        //assertTrue(StatusCode.ORDER_CODES.add("Z"));
        StatusCode.getOrderCodes().sort(Comparator.reverseOrder());
    }
}
