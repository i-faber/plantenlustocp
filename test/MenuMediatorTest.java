import factory.MenuMediatorCreator;
import model.Supplier;
import org.junit.Before;
import org.junit.Test;
import userInterface.menu.MenuMediator;

public class MenuMediatorTest {
    MenuMediator menuMediator;

    @Before
    public void init() {
        menuMediator = MenuMediatorCreator.createForModelData();
    }

//    @Test
//    public void testLoadMenu() {
//        Map<Class, Menu> menus = menuHandler.getMenus();
//        for (Map.Entry entry : menus.entrySet()) {
//            System.out.println(entry.getValue().getClass().getSimpleName());
//        }
//
//        Class associatedType = Supplier.class;
//        Menu menu = menus.get(associatedType);
//        System.out.println(menu.getClass().getSimpleName());
//
//        System.out.println(menus.get(MainMenu.class).getClass().getSimpleName());
//    }

    @Test
    public void testLoadMenu2() {
        menuMediator.loadMenu(Supplier.class);
    }
}
